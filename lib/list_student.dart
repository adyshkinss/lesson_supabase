import 'package:flutter/material.dart';
import 'package:lesson_supabase/main.dart';



class ListStudent extends StatefulWidget {
   ListStudent({super.key});

  @override
  State<ListStudent> createState() => _ListStudentState();
}

class _ListStudentState extends State<ListStudent> {
  final _future = supabase
      .from('students')
      .select<List<dynamic>>();

  Future<dynamic> getStudentList() async {
    List<dynamic> studentList = await supabase
    .from("students")
    .select<List<dynamic>>();

    return studentList;
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text('Список студентов'),        
      ),
      body: SafeArea(child: 
      Padding(
        padding: const EdgeInsets.all(8.0),

        child: FutureBuilder(
        future: getStudentList(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            
            return const Center(child: CircularProgressIndicator());
          }        
          final student = snapshot.data!;

        

        return GridView.builder(
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 200,
                  childAspectRatio: 0.6,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20),
              itemCount: student.length,
              itemBuilder: (BuildContext ctx, index) {
                return Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.cyan,
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [                      
                      Image.network(student[index]["Photo"]),
                      Text(student[index]["LastName"]),
                      Text(student[index]["FirstName"]),
                      Text(student[index]["group_name"]),
                    ],
                  ),
                );
              });
        }),
      ),
      
      ),
      
    );
  }
}