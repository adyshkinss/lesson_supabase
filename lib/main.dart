import 'package:flutter/material.dart';
import 'package:lesson_supabase/list_student.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Supabase.initialize(
    url: 'https://fxjguqccivtdosnrtn.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXnJlZiI6ImZ4amdwcHVxY2NpdnRkb3NucnRuIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTgwNjYwNDYsImV4cCI6MjAxMzY0MjA0Nn0.tT2Jv-inIZ3i5TLkBivBRreuJxH-NFeEYxOut7G8BSU',
    authFlowType: AuthFlowType.pkce,
  );
  runApp(const MyApp());
}


final supabase = Supabase.instance.client;

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Student List',
      theme: ThemeData(
   
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.cyan),
        useMaterial3: true,
      ),
      home: ListStudent(),
    );
  }
}
